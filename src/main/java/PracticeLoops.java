public class PracticeLoops {

    //Write a loop that prints even numbers up to n
    //Expected for 20:  0 2 4 6 8 10 12 14 16 18 20
    public static String printEven20(int n){
        String result = "";
        return result;

    }

    //Write a loop that sums all odd numbers up to n

    public static int sumEven20(int n){
        int sum = 0;
        return sum;

    }

    //write a loop that prints n number of lines with incrementing x's for int n = 5;
    //This question does not have a test, instead use System.out.println();
    /*
        x
        xx
        xxx
        xxxx
        xxxxx
     */
    public static void printX(){
        int n = 5;
        String result = "";

    }

    //Given an array with a length of 10, return odd numbers using foreach loop, separated by a space
    //Expected output for {2, 3, 9, 10, 73, 53, 22, 91, 13, 48};
    //                         3 9 73 53 91 13

    public static String oddArray(int[] arr){
    }

    //given an array of length 10, return the sum of all even numbers using a foreach loop
    //Expected output for {2, 3, 9, 10, 73, 53, 22, 91, 13, 48};
    //                          82

    public static int printEvenSum(int[] arr){
        return 0;
    }

    //given a string, return true if the string starts with "y"

    public static boolean startsY(String word){
        return false;
    }

    //given an array of 10 strings, print all the words that start with x, separated by a space
    //Expected for: {"xhi", "why", "bye", "xlye", "sky", "xfly", "xdie", "xsly", "dye", "pie"}
    //                xhi xlye xfly xdie xsly

    public static String startsX(String[] arr){
        return null;
    }

    //Given a string, remove all zeros
    //Expected for z0er0o  ---> zero

    public static String removeZero(String str){
        return null;
    }


    //Given an array of 10 strings, print strings that start with y with all y removed

    public static String removeY(String[] arr){
        return null;
    }


    //Given a string, count all "oo" in the string. Overlaps count
    //Expected for "woowoowwwoowoooww" ---> 5

    public static int removeOo(String str){
        return 0;
    }



    public static void main(String[] args){
        printX();
    }



}
